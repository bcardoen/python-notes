# Python notes

Basic gotcha's: unlike C/C++ everything (except immutable) is passed by &

## Lists, tuples

### Defining, access
```Python
a = []

a = list("somestring")

elem = a[1]

last = a[-1]

first = a[0]
```
### Slicing

```Python
sublist = a[firstinclusive:lastexclusive:stepsize]

sublist = a[:last]

sublist = a[first:]

sublist = a[2:42:3] # Stepsize can be negative, in which case first>last or use [::-i]

```
### Insert, assign

```Python
a[i:i]=[2,3,4]

```
### Multip, concat

```Python
a = [None]*10

b = [2]

c = [4]

a = b+c

```
### delete

```Python
del a[i]

a[i:i] = []

```
### Operators
```Python
len(a)

max(a)

min(a)

```
### Member functions

```Python
a.reverse() # reverse in place

reversed(a) # returns reverse iterator

a.count() # first occurrence

a.extend(iterable)

a.append(element)

a.pop(index=-1) 

a.index(elem) # index of element or exc.

elem in a # finds but does not return

a.sort() # sort in place

a.sort(key=cmp, reverse=false) # comparison returns a key value on which sequence is sorted
 
```

## Tuples
Immutable array objects. Use as keys in dicts.
### Defining
```Python
a = val,
a = (val, val, ...)
a = tuple(iterable)
```

## Dicts
Keys have to be immutable type


```Python
d = dict(iterable_of_keyvalpairs)
d = {}
len(d) 
d[k] # val @key
del d[k]
k in d

```
### Aliasing:
```Python
a = {}
a[2] = 42
a = b
a.clear() # results in a, b empty
a = {} # results in a empty, b still pointing to original
```
### Member methods
```Python
a.clear()
a.copy() # Shallow
from copy import deepcopy && deepcopy(d) # dcopy
{}.fromkeys([list of keys]) # dict with key:None
dict.fromkeys([list of keys], defaultvalue)
d.get(key) # None instead of exception
d.get(key, defaultipvNone)
d.has_key(key) # equiv to k in d
d.keys
d.items
d.iterkeys # iterator equivalent (more efficient)
d.iteritems
d.pop(item) # remove and return item:val
d.popitem() # remove any (if order is irrelevant, more efficient)
d.update(dict) # add any of dict to d, and overwrite if needed

```

## General usage

###
Standalone executable v modules:
```Python
if __name__ == "__main__": #code

```
Shebang : 
```Python
#!/usr/bin/env python(3)
```

### Short circuit behavior
```Python
a or b  # equivalent to if a : return a, else return b;

```
### Assignment unpacking
```Python
x,y,z = 1,2,4
x,y,z = z,y,x
z,y,x = sequence
x,y, *rest = sequence # x,y assigned first 2, rest all others
```

### Bools
False, None, 0, "", (), [], {}

Equality : ==
Identity : is

Do not use 'is' on immutables, & of immutable may be optimized, cue undefined behavior.

### Conditionals
#### If
```Python
if cond:
    code
elif cond:
    code
else:
    code

```
#### Ternary
```Python
a if b else c # if b == True, return a, else return c
```

#### While
```Python
while cond:
    code
```
#### For

```Python
for elem in sequence:
    code
for elem in range(begin, end, step=1):
    code
for elem in xrange(begin, end, step=1): # xrange : generates item one at a time
for key, value in d.items():

# Parallel iteration
zip(seq, seq) # creates zipped sequence up until shortest
for first, second in zip(seq, seq):

# Indexed iteration
for index, val in enumerate(seq):

# Sort, reverse
sorted(seq) # Sorted copy sequence
reversed(seq) # iterator

# Break, continue and else
for i in xrange(1,100,5):
    code
	if cond:
	    break
else:
    code # Reached if cond was never reached


```
#### List comprehension

```Python
[x for x in seq cond]
[(x,y) for x in seq for y in seq]
```

#### Del
del deletes name, alias, reference, not the actual object

#### exec

```Python
exec(stringcommand) # Do not use
escope = {}
exec string in scope
exec(string, scope)
eval # Used for expressions, returning value
eval(stringexpr)
```

### Functions
```Python
def fname(name[=defvalue],...):
    body
def fname(param, *packed): # packed will be a tuple of all except the first positional parameters
def fname(param, **packed): # packed is a dict of all named parameters
fname(*poslist)
fname(**nameddict)

```
Named parameters:
fname(name=value, name=value) 
Mixing of positional and named is possible, but positional should be first

Nested functions, scoping : use 'global' to override shadowing effect

Nonlocal accessed upperleve scoped variables

Closure : encompassing scope closed by inner function

#### Decorators
A decorator that logs function arguments and return values, with an optional logging object:
```Python
    def traceFunction(fn=None, logcall=None):
        """
        Decorator to log a function with an optional logger.
        Logs arguments and return value of the function object at debug level if no logger is given,
        else uses the logcall object.
        Example usage :
                @traceFunction(logcall=logging.getLogger('global').error) or @traceFunction
                def myfunc(...):
        Based on : https://stackoverflow.com/questions/3888158/python-making-decorators-with-optional-arguments
        """
        if not logcall:
            logcall = logging.getLogger('global').debug

        # This is the wrapping decorator, without kargs
        def _decorate(function):
            @functools.wraps(function)# don't need this, but it is helpful to avoid __name__ overwriting
            # This is the actual decorator, pass arguments and log.
            def inner(*args, **kwargs):
                logcall("Function {} called with pargs {} and kargs {}".format(function, args, kwargs))
                rv = function(*args, **kwargs)
                logcall("Function {} returns {}".format(function, rv))
                return rv
            return inner

        if fn:
            return _decorate(fn)
        return _decorate            
```

#### Functional programming
```Python
map(functor, sequence) # apply f to seq, equiv to [f(i) for i in seq]
filter(functor, sequence) # !remove_if
reduce(func, seq) # apply func to each pair until none are left
lambda param, param : expression # no return !

```

### Classes
use "__metaclass__ = type" # for <3, access to new style classes
```Python
class name[(super,super)]: # multiple inheritance
	# Any code in class definition is executed
	__init(self, ...):

issubclass(classname, classname)
classname.__bases__
isinstance(object, classname)
obj.__class__
__init__(self, ...) # Constructor
SuperclassName.__init__(self, ...) # Superclass constructor
super().__init(self,...)__ # used in single/multi (p3)
super(classname, object) # cname=current, object=self
		
```

Overloading is default in python (omit virtual, as in java), but no overriding, changing function signature with same name leads to redefining, not overriding.

### Exceptions
```Python
raise <x> # Where x is an instance, or a classname (in which case a default constructed instance is generated (class should inherit from Exception)

try:
    statement
except (ExceptionClassName, ...) as e: # "as e is P3, classlist is optional, e as well", try: stat except: stat == equivalent of catchall
    statement
    # e.args # Arguments to exception instance
else:
    statement # Executed if no exception is triggered
finally:
    statement # Always executed
```
#### Raii like behavior
```Python
with open("fname") as e: # e will be closed correctly regardless of what happens in processing
    pass
```

### Modules
```Python
import <modulename>
dir(modulename) # prints namespace defines.
```

### Built ins & inheritance
#### Building sequence objects
```Python
__len__(self):
__getitem__(self,key):
__setitem__(self,key,value):
__delitem__(self,key):
# support slicing by getting slicing parameter as key : slice(start=None, stop, step=None)
```
#### Properties
```Python
# in class def
pname = property(getter, setter=None, deleter=None, docstring=None) # forwarding to accessors __get__, __set__, __del__, ...
```

#### Iterators, Generators
```Python
def __iter__(self): # returns iterator, callable with method __next__(self): # returns 'next' value or StopIteration

# Keyword yield : freezes stackframe of a function in place until resumed
yield <expression> 
def myfunction(self): # generator function
    for value in self.mylist:
	yield value # generator iterator
# Creates a simple iterator-generator type, powerful when combined with recursion
# send(value) -> opens generators up to coroutines
```

### Modules
```Python
import <modulename>
# using sys.path.append("yourpath") or sys.path.expanduser("~/path")
# use a path config file (*.pth), has directories that should be appended to path
# package is directory
dir(modulename) # dir is ls of attrs, in this case scope/space of module
<modulename>.__all__ # defines public interface (names) of a module
# querying functions
print(module.function.__doc__) # nets you the documentation
# get source
print(module.__file__)
 
```

### Quick tour of useful mdos
```Python
import copy # copy, deepcopy
import webbrowser # open("url") -> system defined browser
import set # basic set (std::set), unique membership, unordered, union/isect etc 
# sets are mutable, but may only contain immutable objects (hash fn)
# frozenset is alternative
# heap is set of functions, not an object
deques, defaultdict
```

### Regular expressions
```Python
. # matches single any except \n
# Escaping 
\\<chartoescape> # once from interpreter, second from re module
[abc] or [a-zA-Z] or [^neg]
r'pattern' # raw string, no escaping from interpreter, still once for re module
(pattern1)?pattern2 # matches pattern1pattern2 , but also pattern1
* pattern zero or more
+ pattern one or more
{m,n} pattern m to n times
^pattern # anchors, will only match with pattern at beginning
pattern$ # forces to match entire string
operator? # Nongreedy variant
```

### Other modules
timeit, profile, trace, csv, hashlib, difflib, functools, datetime, logging, itertools, 

